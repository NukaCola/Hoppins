<?php

namespace App\Utils;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class JsonSerializer
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer = null)
    {
        if ($serializer) {
            $this->serializer = $serializer;
        } else {
            $normalizer = new ObjectNormalizer();

            $normalizer->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
                return $object->getName();
            });

            $this->serializer = new Serializer(array($normalizer));
        }
    }

    public function normalize($entity, array $attributes = [])
    {
        return $this->serializer->normalize($entity, 'json', [
			'attributes' => $attributes,
		]);
    }
}
