<?php

namespace App\Admin\Commerce;

use App\Entity\Commerce\Product;
use App\Entity\Commerce\Product\Type;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\ColorSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProductAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('description')
            ->add('visible')
            ->add('enabled')
            ->add('store')
            ->add('type')
            ->add('gender', ChoiceType::class, [
                'choices' => Type::$GENDER,
            ])
            ->add('fabric')
            ->add('color1', ColorSelectorType::class)
            ->add('color2', ColorSelectorType::class)
            ->add('season', ChoiceType::class, [
                'choices' => Product::$SEASONS,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
}
