<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181127142934 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE commerce_brand (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerce_product_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerce_product_fabric (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerce_product_image (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, name VARCHAR(20) NOT NULL, position INT NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_F14DC5E44584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerce_product_type (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, gender SMALLINT NOT NULL, INDEX IDX_92C6104012469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerce_product (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, brand_id INT DEFAULT NULL, store_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, visible TINYINT(1) NOT NULL, enabled TINYINT(1) NOT NULL, price DOUBLE PRECISION DEFAULT NULL, gender SMALLINT NOT NULL, fabric VARCHAR(50) DEFAULT NULL, color1 VARCHAR(10) DEFAULT NULL, color2 VARCHAR(10) DEFAULT NULL, season SMALLINT NOT NULL, INDEX IDX_60C70007C54C8C93 (type_id), INDEX IDX_60C7000744F5D008 (brand_id), INDEX IDX_60C70007B092A811 (store_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerce_store (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, phone_number VARCHAR(15) DEFAULT NULL, siret_number VARCHAR(20) DEFAULT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(50) DEFAULT NULL, INDEX IDX_8FE59FB27E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_management_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', first_name VARCHAR(30) DEFAULT NULL, last_name VARCHAR(30) DEFAULT NULL, birth_date DATETIME DEFAULT NULL, gender TINYINT(1) DEFAULT NULL, phone_number VARCHAR(15) DEFAULT NULL, UNIQUE INDEX UNIQ_BA76DD292FC23A8 (username_canonical), UNIQUE INDEX UNIQ_BA76DD2A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_BA76DD2C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commerce_product_image ADD CONSTRAINT FK_F14DC5E44584665A FOREIGN KEY (product_id) REFERENCES commerce_product (id)');
        $this->addSql('ALTER TABLE commerce_product_type ADD CONSTRAINT FK_92C6104012469DE2 FOREIGN KEY (category_id) REFERENCES commerce_product_category (id)');
        $this->addSql('ALTER TABLE commerce_product ADD CONSTRAINT FK_60C70007C54C8C93 FOREIGN KEY (type_id) REFERENCES commerce_product_type (id)');
        $this->addSql('ALTER TABLE commerce_product ADD CONSTRAINT FK_60C7000744F5D008 FOREIGN KEY (brand_id) REFERENCES commerce_brand (id)');
        $this->addSql('ALTER TABLE commerce_product ADD CONSTRAINT FK_60C70007B092A811 FOREIGN KEY (store_id) REFERENCES commerce_store (id)');
        $this->addSql('ALTER TABLE commerce_store ADD CONSTRAINT FK_8FE59FB27E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_management_user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commerce_product DROP FOREIGN KEY FK_60C7000744F5D008');
        $this->addSql('ALTER TABLE commerce_product_type DROP FOREIGN KEY FK_92C6104012469DE2');
        $this->addSql('ALTER TABLE commerce_product DROP FOREIGN KEY FK_60C70007C54C8C93');
        $this->addSql('ALTER TABLE commerce_product_image DROP FOREIGN KEY FK_F14DC5E44584665A');
        $this->addSql('ALTER TABLE commerce_product DROP FOREIGN KEY FK_60C70007B092A811');
        $this->addSql('ALTER TABLE commerce_store DROP FOREIGN KEY FK_8FE59FB27E3C61F9');
        $this->addSql('DROP TABLE commerce_brand');
        $this->addSql('DROP TABLE commerce_product_category');
        $this->addSql('DROP TABLE commerce_product_fabric');
        $this->addSql('DROP TABLE commerce_product_image');
        $this->addSql('DROP TABLE commerce_product_type');
        $this->addSql('DROP TABLE commerce_product');
        $this->addSql('DROP TABLE commerce_store');
        $this->addSql('DROP TABLE user_management_user');
    }
}
