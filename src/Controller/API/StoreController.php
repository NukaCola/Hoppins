<?php

namespace App\Controller\API;

use App\Entity\Commerce\Product;
use App\Entity\Commerce\Store;
use App\Utils\JsonSerializer;
use App\Utils\UploadRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class StoreController extends Controller
{
	/**
	 * @Route("stores/{id}", name="api.store_show")
	 * @Method("GET")
	 * @return JSonResponse
	 */
	public function show(Store $store, JsonSerializer $serializer)
	{
		$store = $serializer->normalize($store, ['name', 'slug']);

		return new JSonResponse([
			'success' => true,
			'store' => $store,
		]);
	}

	/**
	 * @Route("stores", name="api.store_list")
	 * @Method("GET")
	 * @return JSonResponse
	 */
	public function list(JsonSerializer $serializer)
	{
		$stores = $this->getDoctrine()->getRepository(Store::class)->findAll();

		$stores = $serializer->normalize($stores, ['name', 'slug']);

		return new JSonResponse([
			'success' => true,
			'stores' => $stores,
		]);
	}

	/**
     * Get all products from a store
     *
     * @Route("stores/{id}/products", name="api.store_products")
     * @Method("GET")
     * @return JSonResponse
     */
    public function getProducts(Store $store, JsonSerializer $serializer, UploadRouter $router)
    {
		$products = $store->getProducts();
		foreach ($products as $product) {
			foreach ($product->getImages() as $image) {
				$image->setName($router->generate($image, 'file'));
			}
		}
		$products = $serializer->normalize($products, ['name', 'date', 'description', 'createdAt', 'images' => ['name']]);

		return new JSonResponse([
			'success' => true,
			'products' => $products,
		]);
    }

	/**
	 * @Route("stores/{store_id}/{user_id}/vote", name="api.stores_vote", requirements={"store_id":"\d+", "user_id":"\d+"})
	 * @Method("POST")
	 */
	public function vote()
	{

	}

	/**
	 * Calcul la moyenne des votes pour un magasin
	 *
	 * @Route("stores/{id}/note", name="api.stores_note", requirements={"id":"\d+"})
	 * @Method("GET")
	 *
	 * @param int $id
	 * @return JSonResponse
	 */
	public function note(int $id)
	{
		$userStores = $this->getDoctrine()->getRepository(UserStore::class)->findBy(array('store' => $id));
		$sum = 0;
		$compteur = 0;
		foreach ($userStores as $userStore) {
            $sum += $userStore->getVote();
            $compteur++;
        }
        if($compteur == 0)
        	$note = null;
        else
        	$note = $sum / $compteur;
        return new JSonResponse(["status"=> "success", "note" => $note]);
	}

	/**
	 * @Route("stores/around/{distance}", name="api.stores_around", requirements={"distance":"\d+"})
	 * @Method("GET")
	 */
	public function findStoreAround()
	{
	}
}
