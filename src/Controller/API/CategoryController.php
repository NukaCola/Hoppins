<?php

namespace App\Controller\API;

use App\Entity\Commerce\Product\Category;
use App\Utils\JsonSerializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends Controller
{
	/**
	 * Renvoie la liste de toutes les catégories
	 *
	 * @Route("categories", name="api.categories")
	 * @Method("GET")
	 * @param JsonSerializer
	 * @return JSonResponse
	 */
	public function list(JsonSerializer $serializer)
	{
		$categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

		$categories = $serializer->normalize($categories, ['name', 'slug']);

		return new JSonResponse([
			'success' => true,
			'categories' => $categories,
		]);
	}
}
