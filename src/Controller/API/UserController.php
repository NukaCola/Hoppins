<?php

namespace App\Controller\API;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserController extends Controller
{
    /**
     * @Route("users/login", name="api.get_users")
     * @Method("POST")
     */
    public function login(Request $request)
    {
        $user = json_decode($request->request->get('user'), true);

        $userBDD = $this->getDoctrine()->getRepository(User::class)->findOneByUsernameCanonical($user["emailAddress"]);

        if ($userBDD) {
            if (password_verify($user["password"], $userBDD->getPassword())) {
                $user["id"] = $userBDD->getId();
                $user["token"] = $userBDD->getToken();
                $user["firstName"] = $userBDD->getFirstName();
                $user["lastName"] = $userBDD->getLastName();
                $user["gender"] = $userBDD->getGender();
                $user["birthDate"] = $userBDD->getBirthDate() ? $userBDD->getBirthDate()->format('Y-m-d') : "";

                return new JSonResponse(["status"=> "success", "user" => $user]);
            }
        }

        return new JSonResponse(["status"=> "error"]);
    }

    /**
    * @Route("users", name="api.post_users", methods={"POST", "OPTIONS"})
    */
    public function register(Request $request)
    {
        $user = json_decode($request->request->get('user'), true);

        $request->request->set("fos_user_registration_form", [
            "username" => $user["emailAddress"],
            "email" => $user["emailAddress"],
            "firstName" => $user["firstName"],
            "lastName" => $user["lastName"],
            // "birthDate" => $user["birthDate"],
            "gender" => $user["gender"],
            "plainPassword" => ["first" => $user["password"],
            "second" => $user["password"]]]);

        return $this->forward('FOSUserBundle:Registration:register', array('request' => $request));
    }

    /**
     * @Route("/api/userExist", name="api.userExist")
     * @Method("POST")
     */
    public function getUserExist(Request $request)
    {
        $user = json_decode($request->request->get('user'), true);
        $userBDD = $this->getDoctrine()->getRepository(User::class)->findOneByUsernameCanonical($user["emailAddress"]);

        //$token = $this->getToken($request);
        //$tokenResponse = TokenHelper::checkToken($token, $user);
        if($userBDD){
            return new JSonResponse(["status"=> "success", "userExist" => true]);
        } else {
            return new JSonResponse(["status"=> "success", "userExist" => false]);
        }

        return $this->forward('FOSUserBundle:Registration:register', array('request' => $request));
    }

    /**
     * @Route("/users/{id}", name="api.user", requirements={"id":"\d+"})
     * @Method("GET")
     */
    public function get(string $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        return new JSonResponse([
            "username" => $user->getUsername(),
            "emailAdress" => $user->getEmail()
        ]);
    }

    /**
    * @Route("/users/{id}", name="api.user_update", requirements={"id":"\d+"})
    * @Method("PATCH")
    */
    public function update(Request $request, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $token = $this->getToken($request);
        $tokenResponse = TokenHelper::checkToken($token, $user);

        if ($tokenResponse === true)
        {
            $content = $request->getContent();
            $newUser = json_decode($request->request->get('user'), true);
            if (password_verify($newUser["oldPassword"], $user->getPassword()))
            {
                if(strlen($newUser["newEmailAddress"]) > 0){
                    $user->setEmail($newUser["newEmailAddress"]);
                    $user->setEmailCanonical($newUser["newEmailAddress"]);
                }
                if(strlen($newUser["newEmailAddress"]) > 0){
                    $user->setUsername($newUser["newEmailAddress"]);
                    $user->setUsernameCanonical($newUser["newEmailAddress"]);
                }
                if(strlen($newUser["newPassword"]) > 0){
                    $user->setPassword(password_hash($newUser["newPassword"], PASSWORD_BCRYPT));
                }
                if(strlen($newUser["newFirstName"]) > 0){
                    $user->setFirstName($newUser["newFirstName"]);
                }
                if(strlen($newUser["newLastName"]) > 0){
                    $user->setLastName($newUser["newLastName"]);
                }
                if(strlen($newUser["newGender"]) > 0){
                    $user->setGender($newUser["newGender"]);
                }

                $em->persist($user);
                $em->flush();
                return new JSonResponse(["status"=> "success", "userMailAddress"=> $user->getEmail(), "userFirstName"=> $user->getFirstName(), "userLastName"=> $user->getLastName(), "userGender"=> $user->getGender(), "userPassword"=> $user->getPassword()]);
            }
            return new JSonResponse(["status"=> "error", "error"=> "diffpaswd"]);
        }
        else
            return $tokenResponse;
    }

    /**
    * @Route("/users/{id}", name="api.user_delete", requirements={"id":"\d+"})
    * @Method("DELETE")
    */
    public function delete(Request $request, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository(User::class)->find($id);

        $em->remove($user);
        $em->flush();

        return new JSonResponse(['success'=> true]);
    }

    /**
    * @Route("/users/{id}/products", name="api.user_add_favoris", requirements={"id":"\d+"})
    * @Method("POST")
    */
    public function addFavoris(Request $request, $id)
    {
        $productId = $request->request->get('productId');
        $product = $this->getDoctrine()->getRepository(Product::class)->find($productId);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $token = $this->getToken($request);
        $tokenResponse = TokenHelper::checkToken($token, $user);

        if ($tokenResponse === true)
        {
            $em = $this->getDoctrine()->getEntityManager();
            $user->addProduct($product);
            $em->persist($user);
            $em->flush();

            return new JSonResponse(['success'=> true]);
        }
        else
            return $tokenResponse;
    }

    /**
    * @Route("/users/{user_id}/products/{product_id}", name="api.user_delete_favoris", requirements={"user_id":"\d+", "product_id":"\d+"})
    * @Method("DELETE")
    */
    public function deleteFavorisAction(Request $request, $user_id, $product_id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository(User::class)->find($user_id);

        $product = $em->getRepository(Product::class)->find($product_id);
        if($product) {
            $user->removeProduct($product);
            $em->persist($user);
            $em->flush();

            return new JSonResponse(['success'=> true]);
        }

        return new JSonResponse([
            'success' => false,
            'status' => 404,
        ]);
    }

    /**
    * @Route("/users/{id}/products", name="api.user_get_favoris", requirements={"id":"\d+"})
    * @Method("GET")
    */
    public function getFavoris(Request $request, $id)
    {
        $user = $this->getDoctrine()->getEntityManager()->getRepository(User::class)->find($id);

        $products = $user->getProducts();

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        foreach ($normalizers as $normalizer) {
            $normalizer->setIgnoredAttributes(array('users', 'type', 'timezone', 'owner', 'products'));
            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId();
            });
        }
        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
        foreach ($products as $product) {
            $product->setCreatedAt($product->getCreatedAt()->format('Y-m-d'));
            $product->setUsers(null);
            foreach ($product->getImages() as $image) {
                $path = $helper->asset($image, 'imageFile');
                $image->setImage($path);
            }
        }

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($products, 'json');
        $products = json_decode($jsonContent);

        return new JSonResponse(["status"=> "success", "products"=> $products]);
    }
}
