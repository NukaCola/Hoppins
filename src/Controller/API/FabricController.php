<?php

namespace App\Controller\API;

use App\Entity\Commerce\Product\Fabric;
use App\Utils\JsonSerializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FabricController extends Controller
{
	/**
	 * Renvoie la liste de toutes les matières
	 *
	 * @Route("fabrics", name="api.fabrics")
	 * @Method("GET")
	 * @param JsonSerializer
	 * @return JSonResponse
	 */
	public function list(JsonSerializer $serializer)
	{
		$fabrics = $this->getDoctrine()->getRepository(Fabric::class)->findAll();

		$fabrics = $serializer->normalize($fabrics, ['name', 'slug']);

		return new JSonResponse([
			'success' => true,
			'fabrics' => $fabrics,
		]);
	}

}
