<?php

namespace App\Controller\API;

use App\Entity\Commerce\Product\Type;
use App\Utils\JsonSerializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class TypeController extends Controller
{
	/**
	 * Renvoie la liste de tous les types
	 *
	 * @Route("types", name="api.types")
	 * @Method("GET")
	 */
	public function list(JsonSerializer $serializer)
	{
		$types = $this->getDoctrine()->getRepository(Type::class)->findAll();

		$types = $serializer->normalize($types, ['name', 'slug', 'gender']);

		return new JSonResponse([
			'success' => true,
			'types' => $types,
		]);
	}
}
