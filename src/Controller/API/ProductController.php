<?php

namespace App\Controller\API;

use App\Entity\Commerce\Product;
use App\Entity\Commerce\Product\Category;
use App\Entity\Commerce\Product\Type;
use App\Entity\Commerce\Store;
use App\Utils\JsonSerializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

// TODO: Remvoe
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ProductController extends Controller
{
	/**
	 * Récupère une id et renvoie le produit correspondant
	 *
	 * @Route("products/{id}", name="api.products_show", requirements={"id":"\d+"})
	 * @Method("GET")
	 * @param int $id, Request $request
	 * @return JSonResponse
	 */
	public function show(Request $request, Product $product)
	{
		$helper = $this->get('vich_uploader.templating.helper.uploader_helper');
		foreach ($product->getImages() as $image) {
			$path = $helper->asset($image, 'file');
			$imageUrl = isset($path) ? $request->getUriForPath($path) : null;
			$image->setName($imageUrl);
		}

		$serializer = new JsonSerializer;
		$product = $serializer->normalize($product, ['name', 'images' => ['name']]);

		return new JSonResponse([
			'success' => true,
			'product' => $product,
		]);
	}

	/**
	 * Tous les produits
	 *
	 * @Route("products", name="api.products", methods={"GET"})
	 * @return JSonResponse
	 */
	public function list(Request $request)
	{
		$products = $this->getDoctrine()->getRepository(Product::class)->findAll();

		$helper = $this->get('vich_uploader.templating.helper.uploader_helper');
		foreach ($products as $product) {
			foreach ($product->getImages() as $image) {
				$path = $helper->asset($image, 'file');
				$imageUrl = isset($path) ? $request->getUriForPath($path) : null;
				$image->setName($imageUrl);
			}
		}

		$serializer = new JsonSerializer;
		$products = $serializer->normalize($products, ['name', 'images' => ['name']]);

		return new JSonResponse([
			'success' => true,
			'products' => $products,
		]);
	}

	/**
	 * Récupère une id et renvoie le produit correspondant
	 *
	 * @Route("products", name="api.products_create", methods={"POST", "OPTIONS"})
	 * @param Request $request
	 * @return JSonResponse
	 */
	public function create(Request $request)
	{
		return new JSonResponse('ok');
	}

    private function getAddressGPSInfos(String $userCity){
        $curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => "https://geocoder.api.here.com/6.2/geocode.json?app_id=SyEinQODdSojUHwoJfyw&app_code=jaABEOWUn-gdMe0qGd-O_A&searchtext=" . $userCity,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "GET",
		]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }

    /**
     * Optimized algorithm from http://www.codexworld.com
     *
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     * @return float [km]
     */
    private function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $rad = M_PI / 180;
        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin($latitudeFrom * $rad)
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

        return acos($dist) / $rad * 60 *  1.853;
    }

	/**
	 * Search by word(name, description, gender, saison),fabric,type
	 *
	 * @Route("fullsearch", name="api.full_recherche")
	 * @Method("POST")
	 * @param Request $request
	 * @return JSonResponse
	 */
	public function fullSearchAction(Request $request)
	{
		$research = json_decode($request->request->get('search'), true);

		$em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()->select('p')->from(Product::class, 'p');

		if(strlen($research["wordSearch"]) > 0){
			$title = "%" . $research["wordSearch"] . "%";

            $query->leftJoin(Store::class, 's', 'WITH', 'p.store = s.id');
            $orModule = $query->expr()->orx();
            $orModule->add($query->expr()->like('p.name', ':name'));
            $orModule->add($query->expr()->like('p.description', ':description'));
            $orModule->add($query->expr()->like('p.gender', ':genderWord'));
            $orModule->add($query->expr()->like('p.saison', ':saison'));
            $orModule->add($query->expr()->like('s.name', ':storeName'));

            $query->andWhere($orModule)->setParameter('name', $title)->setParameter('description', $title)->setParameter('genderWord', $title)->setParameter('saison', $title)->setParameter('storeName', $title);
		}

		if(strlen($research["gender"]) > 0){
            if($research["gender"] != "mixte"){
                $query->andWhere('p.gender = :gender')->setParameter('gender', $research["gender"]);
            }
		}

		if(strlen($research["fabricSlug"]) > 0){
			$query->andWhere('p.tissu LIKE :tissu')->setParameter('tissu', $research["fabricSlug"]);
		}

        if(strlen($research["typeID"]) > 0){
            $query->andWhere('p.type = :typeID')->setParameter('typeID', $research["typeID"]);
        } else if(sizeof($research["categoryIDs"]) > 0){
			$categoryIDs = $this->getDoctrine()->getRepository(Category::class)->findBy(array('id' => $research["categoryIDs"]));
			$typeIDs = $this->getDoctrine()->getRepository(Type::class)->findBy(array('category' => $categoryIDs));
			$query->andWhere('p.type IN (:typeIDs)')->setParameter('typeIDs', $typeIDs);
		}

		if(sizeof($research["colorsIDs"]) > 0){
			$query->andWhere('p.color1 IN (:couleurOne) OR p.color2 IN (:couleurTwo)')->setParameter('couleurOne', $research["colorsIDs"])->setParameter('couleurTwo', $research["colorsIDs"]);
		}

		$finalProducts = $query->getQuery()->getResult();

		// ********** FINAL PART *************

		$encoders = array(new JsonEncoder());
		$normalizers = array(new ObjectNormalizer());

		foreach ($normalizers as $normalizer) {
			$normalizer->setIgnoredAttributes(array('users', 'timezone', 'owner', 'products', 'type'));
			$normalizer->setCircularReferenceHandler(function ($object) {
				return $object->getId();
			});
		}

		// $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
		// foreach ($finalProducts as $finalProduct) {
		// 	// $finalProduct->setCreatedAt($finalProduct->getCreatedAt()->format('Y-m-d'));
		// 	foreach ($finalProduct->getImages() as $image) {
		// 		$path = $helper->asset($image, 'imageFile');
		// 		$image->setImage($path);
		// 	}
		// }

        // --------------- Around User Sort -----------------

        $storesAround = $this->getDoctrine()->getRepository(Store::class)->findAll();
        $aroundProducts = [];

        foreach ($storesAround as $storeAround) {
            $storeAddressStr = $storeAround->getAddress() . " " . $storeAround->getCity();
            $storeAddressStr = str_replace(" ","+",$storeAddressStr);
            $storeAddressStr = strtr($storeAddressStr, "àäâéèêëïîöôùüû", "aaaeeeeiioouuu");

            $storeCoord = $this->getAddressGPSInfos($storeAddressStr);
            $storeLat = $storeCoord["Response"]["View"][0]["Result"][0]["Location"]["DisplayPosition"]["Latitude"];
            $storeLong = $storeCoord["Response"]["View"][0]["Result"][0]["Location"]["DisplayPosition"]["Longitude"];

            if($this->codexworldGetDistanceOpt($research["userLat"], $research["userLong"], $storeLat, $storeLong) < $research["distanceSearch"]){
                foreach ($finalProducts as $finalProduct) {
                    if($finalProduct->getStore()->getId() == $storeAround->getId()){
                        $aroundProducts[] = $finalProduct;
                    }
                }
            }
        }

        $finalProductsCount = count($aroundProducts);

        $partOfAroundProducts = array_slice($aroundProducts, $research["lastIndex"], $research["resultsNb"]);

        $partOfAroundProductsCount = count($partOfAroundProducts);

        if(count($partOfAroundProducts) < $research["resultsNb"]){
            $lastPage = true;
        } else {
            $lastPage = false;
        }

		$serializer = new Serializer($normalizers, $encoders);
		$jsonContent = $serializer->serialize($partOfAroundProducts, 'json');
		$products = json_decode($jsonContent);

		return new JSonResponse(["status"=> "success", "productsCount" => $partOfAroundProductsCount, "lastPage" => $lastPage, "products"=> $products]);
	}

    /**
     * Get 3 product from a store
     *
     * @Route("storeProductsSample/{store_id}", name="api.store_products_sample")
     * @Method("GET")
     * @return JSonResponse
     */
    public function storeProductsSample($store_id)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()->select('p')->from('App\Entity\Product', 'p');
        $query->andWhere('p.store = :storeID')->setParameter('storeID', $store_id);
        $query->setMaxResults(3);

        $finalProducts = $query->getQuery()->getResult();

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        foreach ($normalizers as $normalizer) {
            $normalizer->setIgnoredAttributes(array('users', 'timezone', 'owner', 'products', 'type'));
			$normalizer->setCircularReferenceHandler(function ($object) {
				return $object->getId();
			});
        }

        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
        foreach ($finalProducts as $finalProduct) {
            $finalProduct->setCreatedAt($finalProduct->getCreatedAt()->format('Y-m-d'));
            foreach ($finalProduct->getImages() as $image) {
                $path = $helper->asset($image, 'imageFile');
                $image->setImage($path);
            }
        }

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($finalProducts, 'json');
        $products = json_decode($jsonContent);

        return new JSonResponse(["status"=> "success", "products"=> $products]);
    }
}
